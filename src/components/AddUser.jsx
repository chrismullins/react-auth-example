import React from 'react';

const AddUser = (props) => {
	return (
<form onSubmit={(event) => props.addUser(event)}>
  <div className="form-row align-items-center">
    <div className="col-auto">
      <label className="sr-only" htmlFor="inlineFormInput">Email</label>
      <input name="email"
             required
             value={props.email}
             onChange={props.handleChange}
             type="email" 
             className="form-control mb-2 mb-sm-0" 
             id="inlineFormInput" 
             placeholder="janedoe@example.com"
      />
    </div>
    <div className="col-auto">
      <label className="sr-only" htmlFor="inlineFormInputGroup">Username</label>
      <div className="input-group mb-2 mb-sm-0">
        <div className="input-group-addon">@</div>
        <input name="username" 
               type="text"
               required
               value={props.username}
               onChange={props.handleChange}
               className="form-control" 
               id="inlineFormInputGroup" 
               placeholder="Username"></input>
      </div>
    </div>
    <div className="col-auto">
      <button type="submit" className="btn btn-primary">Submit</button>
    </div>
  </div>
</form>
	)
}

export default AddUser;